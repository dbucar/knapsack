package solvers;

import utility.Maths;
import utility.Printer;

public class Backtrack2 extends Solver{
	
	protected void go(){
		Printer.printLineToFile("\n============= BT2 =============\n");
		Knapsack(0, 0);
	}
	
	private void Knapsack(int i, int currW){
		
		// exclude the root and "leaf" nodes
		iterCounter++;
		
		// all items checked
		if (i == X.length){
			int currP = Maths.product(X, profits);
			if (currP > bestP){
				bestP = currP;
				bestW = currW;
				System.arraycopy(X, 0, bestX, 0, X.length);
			}
		}
		else{
			if (currW + weights[i] <= W){
				X[i] = true;
				Knapsack(i+1, currW + weights[i]);
			}	
			X[i] = false;
			Knapsack(i+1, currW);
		}
	}

}
