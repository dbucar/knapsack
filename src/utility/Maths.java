package utility;

public class Maths {

	public static int product(boolean[] x, int[] c) {
		int total = 0; 
		for (int i = 0; i < x.length; i++){
			total += (x[i] ? c[i] : 0);
		}
		return total;
	}
}
