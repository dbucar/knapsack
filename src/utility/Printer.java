package utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import objects.Data;
import solvers.Solver;

public class Printer {
	
	private static String outFile = "output/out_gui.txt"; 

	public static void printX(boolean[] x, String what) {
		System.out.print(what + ":\t");
		for (boolean b : x) {
			System.out.print((b ? 1 : 0) + "\t");
		}
		System.out.println();
	}

	public static void printValues(int[] c, String what) {
		System.out.print(what + ":\t");
		for (int i : c) {
			System.out.print(i + "\t");
		}
		System.out.println();
	}
	
	public static void printLineToFile(String line){
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outFile), true));
			bw.write(line + "\n");
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void printXtoFile(boolean[] x, String what) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outFile), true));
			StringBuilder sb = new StringBuilder();
			for (boolean b : x) {
				sb.append((b ? 1 : 0) + "\t");
			}
			bw.write(what + ":\t" + sb.toString() + "\n");
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void printValuestoFile(int[] v, String what){
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outFile), true));
			StringBuilder sb = new StringBuilder();
			for (int i : v) {
				sb.append(i + "\t");
			}
			bw.write(what + ":\t" + sb.toString() + "\n");
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String getSummary(Data d, Solver s){
		
		StringBuilder sb = new StringBuilder();
		//sb.append("Capacity:\t" + d.capacity + "\n");
		//sb.append("#Items:\t" + d.numItems + "\n");
		
		sb.append("Weights:\t");
		for (int i : d.weights) {
			sb.append(i + "\t");
		}
		sb.append("\n");
		
		sb.append("Profits:\t");
		for (int i : d.profits) {
			sb.append(i + "\t");
		}
		sb.append("\n");
		
		sb.append("Selected:\t");
		for (boolean b : s.bestX) {
			sb.append((b ? 1 : 0) + "\t");
		}
		sb.append("\n");
		
		sb.append("Total profit:\t\t" + s.bestP + "\n");
		sb.append("Total weight:\t" + s.bestW + "\n");
		sb.append("Time:\t\t\t" + s.time + "ms\n");
		
		return sb.toString();
		
	}
}
