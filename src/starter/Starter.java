package starter;

import objects.Data;
import solvers.HillClimb;
import solvers.Solver;
import solvers.Backtrack1;
import solvers.Backtrack2;
import utility.InstanceGenerator;
import utility.Loader;
import utility.Printer;

public class Starter {

	private static int runs = 5;

	// stats
	private static double avgTime1, avgTime2, avgIter1, avgIter2, avgProfit1, avgProfit2, solQuality;
	private static long maxTime1, minTime1, maxTime2, minTime2;
	private static long maxIter1, minIter1, maxIter2, minIter2;
	private static int maxProfit1, minProfit1, maxProfit2, minProfit2;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Data d = Loader.load(args[0]);

		int[] numItems = { 100, 120, 150, 170 };
		int[] capacity = { 1000, 2000, 5000 };

		Solver b2;
		Solver hc = null;

		for (int ni : numItems) {
			for (int c : capacity) {
				int run = 0;
				resetStats();
				while (run++ < runs) {
					Data d = InstanceGenerator.generate(ni, c);

					// Solver b1 = new Backtrack1();
					// b1.start(d);

					b2 = new Backtrack2();
					b2.start(d);

					hc = new HillClimb();
					hc.start(d);

					updateStats(b2, hc);

				}
				avgTime1 /= runs;
				avgIter1 /= runs;
				// avgProfit1 /= runs;
				avgTime2 /= runs;
				avgIter2 /= runs;
				// avgProfit2 /= runs;
				solQuality /= runs;

				printStats(ni, c, hc);
			}
		}

	}

	private static void resetStats() {
		avgTime1 = 0;
		maxTime1 = 0;
		minTime1 = Long.MAX_VALUE;
		avgTime2 = 0;
		maxTime2 = 0;
		minTime2 = Long.MAX_VALUE;

		avgIter1 = 0;
		maxIter1 = 0;
		minIter1 = Long.MAX_VALUE;
		avgIter2 = 0;
		maxIter2 = 0;
		minIter2 = Long.MAX_VALUE;

		// avgProfit1 = 0;
		// maxProfit1 = 0;
		// minProfit1 = Integer.MAX_VALUE;
		// avgProfit2 = 0;
		// maxProfit2 = 0;
		// minProfit2 = Integer.MAX_VALUE;

		solQuality = 0;

	}

	private static void updateStats(Solver s1, Solver s2) {

		// time1
		avgTime1 += s1.time;
		if (s1.time < minTime1)
			minTime1 = s1.time;
		if (s1.time > maxTime1)
			maxTime1 = s1.time;

		// iter1
		avgIter1 += s1.iterCounter;
		if (s1.iterCounter < minIter1)
			minIter1 = s1.iterCounter;
		if (s1.iterCounter > maxIter1)
			maxIter1 = s1.iterCounter;

		// profit1
		// avgProfit1 += s.bestP;
		// if (s.bestP < minProfit1)
		// minProfit1 = s.bestP;
		// if (s.bestP > maxProfit1)
		// maxProfit1 = s.bestP;

		// time2
		avgTime2 += s2.time;
		if (s2.time < minTime2)
			minTime2 = s2.time;
		if (s2.time > maxTime2)
			maxTime2 = s2.time;

		// iter2
		avgIter2 += s2.iterCounter;
		if (s2.iterCounter < minIter2)
			minIter2 = s2.iterCounter;
		if (s2.iterCounter > maxIter2)
			maxIter2 = s2.iterCounter;

		// profit2
		// avgProfit2 += s.bestP;
		// if (s.bestP < minProfit2)
		// minProfit2 = s.bestP;
		// if (s.bestP > maxProfit2)
		// maxProfit2 = s.bestP;

		solQuality += s2.bestP / (double) s1.bestP;

	}

	private static void printStats(int ni, int c, Solver hc) {

		Printer.printLineToFile("*** Instance " + ni + " " + c + " stats ***");
		Printer.printLineToFile("-- BT2 --");
		Printer.printLineToFile("Time:: Avg:" + avgTime1 + "ms | Max:" + maxTime1 + "ms | Min:" + minTime1
				+ "ms");
		Printer.printLineToFile("Iterations:: Avg:" + avgIter1 + " | Max:" + maxIter1 + " | Min:" + minIter1);

		Printer.printLineToFile("-- HC --");
		Printer.printLineToFile("Time:: Avg:" + avgTime2 + "ms | Max:" + maxTime2 + "ms | Min:" + minTime2
				+ "ms");
		Printer.printLineToFile("Iterations:: Avg:" + avgIter2 + " | Max:" + maxIter2 + " | Min:" + minIter2);
		Printer.printLineToFile("Best solo run:: " + hc.bestT);
		Printer.printLineToFile("Solution quality (P/P*):: " + solQuality);

	}

}
