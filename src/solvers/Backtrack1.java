package solvers;

import utility.Maths;
import utility.Printer;

public class Backtrack1 extends Solver {

	protected void go() {
		Printer.printLineToFile("\n============= BT1 =============\n");
		Knapsack(0);
	}

	private void Knapsack(int i) {

		// exclude the root and "leaf" nodes
		iterCounter++;
		int currW;
		
		// all items checked
		if (i == X.length) {
			currW = Maths.product(X, weights);
			if (currW <= W) {
				int currP = Maths.product(X, profits);
				if (currP > bestP) {
					bestP = currP;
					bestW = currW;
					System.arraycopy(X, 0, bestX, 0, X.length);
				}
			}
		}

		else {
			X[i] = true;
			Knapsack(i + 1);
			X[i] = false;
			Knapsack(i + 1);
		}
	}

}
