package utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import objects.Data;

public class Loader {
	
	public static Data load (String path){
		
		Data data = new Data();
		BufferedReader br = null;
		
		try {
			
			br = new BufferedReader(new FileReader(new File(path)));
			
			// #items and capacity
			String line = br.readLine();
			String[] split = line.split("\\s");
			data.numItems = Integer.parseInt(split[0]);
			data.capacity = Integer.parseInt(split[1]);
			
			// weights
			line = br.readLine(); 
			split = line.split("\\s");
			data.weights = new int[split.length];
			for (int i = 0; i < split.length; i++){
				data.weights[i] = Integer.parseInt(split[i]);
			}
			
			// profits
			line = br.readLine();
			split = line.split("\\s");
			data.profits = new int[split.length];
			for (int i = 0; i < split.length; i++){
				data.profits[i] = Integer.parseInt(split[i]);
				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return data;
	}
}
