package objects;

public class Knapsack {

	public boolean[] taken;
	public int weight;
	public int profit;

	public Knapsack(boolean[] x, int kWeight, int kProfit) {
		taken = new boolean[x.length];
		System.arraycopy(x, 0, taken, 0, x.length);
		weight = kWeight;
		profit = kProfit;
	}

	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("Selected:\t");
		for (boolean b : taken) {
			sb.append((b ? 1 : 0) + "\t");
		}
		sb.append("\nTotal profit:\t" + profit + "\n");
		sb.append("Total weight:\t" + weight);

		return sb.toString();
	}
}
