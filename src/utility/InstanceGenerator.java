package utility;

import objects.Data;

public class InstanceGenerator {

	public static final int maxProfit = 1000; 
	
	public static Data generate(int numItems, int capacity) {

		Data d = new Data();
		d.numItems = numItems;
		d.capacity = capacity;
		d.profits = new int[numItems];
		d.weights = new int[numItems];

		for (int i = 0; i < numItems; i++) {
			d.profits[i] = (int) (1 + Math.random() * maxProfit);
			d.weights[i] = (int) (1 + Math.random() * capacity);
		}

		return d;
	}
}
