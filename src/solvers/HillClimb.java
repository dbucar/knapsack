package solvers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import objects.Knapsack;
import utility.Printer;

public class HillClimb extends Solver {

	private int restarts = 100;

	@Override
	protected void go() {
		Printer.printLineToFile("\n============= HC =============\n");
		climb();
	}

	private void climb() {

		Knapsack K;
		Knapsack nextK;
		Knapsack bestK = null;

		// random restarts
		while (restarts-- > 0) {

			// initialize
			K = generateRandomSolution();
			// System.out.println(K.toString());
			boolean improved = true;
			int iterSingle = 0;
			long time = System.currentTimeMillis();

			// climbing
			while (improved) {
				nextK = steepestAscent(K);
				if (nextK != null) {
					K = nextK;
					// System.out.println(K.toString());
				} else {
					improved = false;
				}
				iterSingle++;
				iterCounter++;
			}

			time = System.currentTimeMillis() - time;
			// System.out.println("1run iter (time):\t" + iter + " (" + time + "ms)");

			if (bestK == null || K.profit > bestK.profit) {
				bestK = K;
				bestT = time + "ms (" + iterSingle + " steps)"; 
				//System.out.println("TOTAL BEST\n" + bestK.toString());
				//System.out.println("----------------------------------");
			}
		}

		// assign result to global X
		bestX = bestK.taken;
		bestP = bestK.profit;
		bestW = bestK.weight;

	}

	/*
	 * returns null if no improving solution found
	 */
	private Knapsack steepestAscent(Knapsack k) {

		boolean[] oldX = k.taken; // just a reference
		int oldP = k.profit;
		int oldW = k.weight;

		// get 1s' and 0s' indices
		List<Integer> trueTakes = new ArrayList<Integer>();
		List<Integer> falseTakes = new ArrayList<Integer>();

		for (int i = 0; i < oldX.length; i++) {
			if (oldX[i]) {
				trueTakes.add(i);
			} else {
				falseTakes.add(i);
			}
		}

		int newW, newP;
		int bestW = oldW, bestP = oldP;
		int bestTFlip = 0, bestFFlip = 0;
		boolean improved = false;

		// check the neighbors - try all combinations of 1-0/0-1 2-flips
		// O(n^2) worst case
		for (int tTakeIdx : trueTakes) {
			for (int fTakeIdx : falseTakes) {
				newW = oldW - weights[tTakeIdx] + weights[fTakeIdx];
				if (newW <= W) { // if 2-flip feasible
					newP = oldP - profits[tTakeIdx] + profits[fTakeIdx];
					// if larger P or same P but reduced W
					if (newP > bestP || (newP == bestP && newW < bestW)) {
						bestTFlip = tTakeIdx;
						bestFFlip = fTakeIdx;
						bestP = newP;
						bestW = newW;
						improved = true;
					}
				}
			}
		}

		Knapsack newK = null;

		if (improved) {
			// flip the selected bits
			oldX[bestTFlip] = false;
			oldX[bestFFlip] = true;
			//System.out.println("Flipping " + bestTFlip + " - " + bestFFlip);
			//System.out.println(bestP + " " + bestW);

			// check if you can add any extra items - randomized greedy
			Collections.shuffle(falseTakes);
			for (int toTrue : falseTakes) {
				if (toTrue != bestFFlip) {
					newW = bestW + weights[toTrue];
					if (newW <= W) {
						bestW = newW;
						bestP += profits[toTrue];
						oldX[toTrue] = true;
						//System.out.println("Flipping " + toTrue);
						//System.out.println(bestP + " " + bestW);
					}
				}
			}

			newK = new Knapsack(oldX, bestW, bestP);
		}

		// will be null if not improved
		return newK;
	}

	/*
	 * generates a random feasible solution
	 */
	private Knapsack generateRandomSolution() {

		List<Integer> availableInd = new ArrayList<Integer>();
		for (int i = 0; i < X.length; i++) {
			availableInd.add(i);
		}

		Collections.shuffle(availableInd);

		int kWeight = 0, kProfit = 0;

		for (int idx : availableInd) {
			if (kWeight + weights[idx] <= W) { // feasible
				X[idx] = true;
				kProfit += profits[idx];
				kWeight += weights[idx];
			} else {
				X[idx] = false;
			}
		}

		return new Knapsack(X, kWeight, kProfit);
	}

}
