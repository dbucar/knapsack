package solvers;

import objects.Data;
import utility.Maths;
import utility.Printer;

public abstract class Solver {

	public long time;
	protected boolean[] X;
	// for Bactrack1&2 this is the optimum
	public boolean[] bestX;
	public int bestP;
	public int bestW;
	public String bestT;

	protected int W;
	protected int[] weights, profits;

	public long iterCounter;

	public void start(Data d) {

		W = d.capacity;
		weights = d.weights;
		profits = d.profits;

		X = new boolean[d.numItems];
		bestX = new boolean[d.numItems];
		bestP = 0;
		iterCounter = 0;

		time = System.currentTimeMillis();
		go();
		time = System.currentTimeMillis() - time;

		//printOut(time);
		printToFile(time);

	}

	private void printOut(long time) {
		System.out.println("Capacity:\t" + W);
		System.out.println("#Items:\t\t" + bestX.length);
		 Printer.printValues(weights, "Weights");
		 Printer.printValues(profits, "Profits");
		 Printer.printX(bestX, "Selected");
		 System.out.println("Total profit:\t" + bestP);
		 System.out.println("Total weight:\t" + Maths.product(bestX,
		 weights));
		if (bestT == null) {
			System.out.println("Time:\t\t" + time + "ms");
			System.out.println("Iterations:\t" + iterCounter);
		} else
			System.out.println("Best time:\t" + bestT);

	}

	private void printToFile(long time) {
		Printer.printLineToFile("Capacity:\t" + W);
		Printer.printLineToFile("#Items:\t\t" + bestX.length);
		Printer.printValuestoFile(weights, "Weights");
		Printer.printValuestoFile(profits, "Profits");
		Printer.printXtoFile(bestX, "Selected");
		Printer.printLineToFile("Total profit:\t" + bestP);
		Printer.printLineToFile("Total weight:\t" + bestW);
		Printer.printLineToFile("Time:\t\t" + time + "ms");
		if (bestT == null)
			Printer.printLineToFile("Iterations:\t" + iterCounter);
		else
			Printer.printLineToFile("Best time:\t" + bestT);
		Printer.printLineToFile("----------------------------------");
	}

	protected abstract void go();

}
