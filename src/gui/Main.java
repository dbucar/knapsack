package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;

import net.miginfocom.swing.MigLayout;
import objects.Data;
import solvers.Backtrack2;
import solvers.HillClimb;
import solvers.Solver;
import utility.InstanceGenerator;
import utility.Printer;

public class Main implements ActionListener{

	private JFrame frmKnapsackProblem;
	private JSpinner txtFieldNumItems;
	private JSpinner txtFieldCap;
	private JButton btnStart;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmKnapsackProblem.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frmKnapsackProblem = new JFrame();
		frmKnapsackProblem.setType(Type.POPUP);
		frmKnapsackProblem.setTitle("Knapsack problem");
		frmKnapsackProblem.setBounds(100, 100, 500, 450);
		frmKnapsackProblem.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKnapsackProblem.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panelHeader = new JPanel();
		frmKnapsackProblem.getContentPane().add(panelHeader, BorderLayout.NORTH);
		panelHeader.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JToolBar toolBarParam = new JToolBar();
		toolBarParam.setFloatable(false);
		toolBarParam.setToolTipText("Parameters");
		toolBarParam.setOrientation(SwingConstants.VERTICAL);
		panelHeader.add(toolBarParam);
		
		JPanel panelParam = new JPanel();
		toolBarParam.add(panelParam);
		panelParam.setLayout(new MigLayout("", "[32px][70px]", "[25.00px][25.00]"));
		
		JLabel lblItems = new JLabel("Items:");
		lblItems.setHorizontalTextPosition(SwingConstants.CENTER);
		lblItems.setHorizontalAlignment(SwingConstants.CENTER);
		panelParam.add(lblItems, "cell 0 0,alignx right,aligny center");
		
		txtFieldNumItems = new JSpinner();
		txtFieldNumItems.setPreferredSize(new Dimension(70, 20));
		txtFieldNumItems.setModel(new SpinnerNumberModel(5, 5, 9999, 5));
		panelParam.add(txtFieldNumItems, "cell 1 0,alignx left,aligny center");
		
		JLabel lblCapacity = new JLabel("Capacity:");
		lblCapacity.setHorizontalAlignment(SwingConstants.LEFT);
		lblCapacity.setHorizontalTextPosition(SwingConstants.LEFT);
		panelParam.add(lblCapacity, "cell 0 1,alignx right,aligny center");
		
		txtFieldCap = new JSpinner();
		txtFieldCap.setPreferredSize(new Dimension(70, 20));
		txtFieldCap.setModel(new SpinnerNumberModel(50, 50, 9999, 50));
		panelParam.add(txtFieldCap, "cell 1 1,alignx left,aligny center");
		
		btnStart = new JButton("Start");
		btnStart.addActionListener(this);
		btnStart.setPreferredSize(new Dimension(87, 25));
		panelHeader.add(btnStart);
		
		//JPanel panelContent = new JPanel();
		
		textArea = new JTextArea();
		textArea.setTabSize(3);
		textArea.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(textArea);
		
		frmKnapsackProblem.getContentPane().add(scrollPane, BorderLayout.CENTER);
	}

	/**
     * Invoked when the user presses the Start button.
     */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// disable gui
		btnStart.setEnabled(false);
		txtFieldNumItems.setEnabled(false);
		txtFieldCap.setEnabled(false);
		textArea.setText("Please wait...");
		
		AlgRunner ar = new AlgRunner(txtFieldNumItems.getValue(), txtFieldCap.getValue());
		ar.execute();
	}
	
	class AlgRunner extends SwingWorker<Void, Void> {
		
		private int numItems, capacity;
		private String strResult;

		public AlgRunner(Object oNumItems, Object oCapacity) {
			numItems = (Integer) oNumItems;
			capacity = (Integer) oCapacity;
		}

		/*
	     * Main task. Executed in background thread.
	     */
		@Override
		protected Void doInBackground() throws Exception {
			
			Data d = InstanceGenerator.generate(numItems, capacity);
			
			Solver bt = new Backtrack2();
			bt.start(d);
			
			String resultBT = "== Backtrack2 ==\n" + Printer.getSummary(d, bt);
			
			Solver hc = new HillClimb();
			hc.start(d);
			
			String resultHC = "== HillClimb ==\n" + Printer.getSummary(d, hc);
			
			strResult = resultBT + "\n" + resultHC;
			
			return null;
		}
		
		/*
	     * Executed in event dispatching thread
	     */
	    @Override
	    public void done() {
	    	textArea.setText(strResult);
	    	
	    	// enable gui
			btnStart.setEnabled(true);
			txtFieldNumItems.setEnabled(true);
			txtFieldCap.setEnabled(true);
	    }

	}

}
